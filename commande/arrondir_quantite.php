<?php

/**
 * Fonctions du plugin Commandes relatives à la référence de commande
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Commandes
 */


// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Arrondir la quantite d'une ligne de commande, en fonction de ce que permet l'objet
 * par defaut c'est en entier pour tout le monde, mais un plugin peut etendre cela
 * @api
 *
 * @param $quantite
 * @param string $objet
 * @param int $id_objet
 * @return int
 *   Retourne la quantité arrondie
 */
function commande_arrondir_quantite_dist($quantite, $objet = '', $id_objet = 0) {

	return round($quantite,3);
}
