<?php
/**
 * Fonctions du plugin Commandes
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Afficher la quantite si differente de 1
 * @filtre
 *
 * @param int|float $quantite
 * @param string $objet
 * @param int $id_objet
 * @return string
 */
function filtre_commandes_afficher_quantite_descriptif($quantite, $objet = '', $id_objet = 0) {
	if (intval($quantite*1000) !== 1000) {
		$commande_arrondir_quantite = charger_fonction('arrondir_quantite', 'commande');
		return $commande_arrondir_quantite($quantite, $objet, $id_objet) . ' &times; ';
	}
	return '';
}

/**
 * Afficher la quantite, en arrondissant eventuellement
 * (par defaut fait juste l'arrondi int natif)
 * @filtre
 *
 * @param int|float $quantite
 * @param string $objet
 * @param int $id_objet
 * @return string
 */
function filtre_commandes_afficher_quantite($quantite, $objet = '', $id_objet = 0) {
	if (intval($quantite*1000) === 1000 * intval($quantite)) {
		return intval($quantite);
	}

	$commande_arrondir_quantite = charger_fonction('arrondir_quantite', 'commande');
	return $commande_arrondir_quantite($quantite, $objet, $id_objet);
}
